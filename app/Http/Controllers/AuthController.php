<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function finis(Request $request)
{
  $data = $request->all();
  return view("finis", $data);
}
}
